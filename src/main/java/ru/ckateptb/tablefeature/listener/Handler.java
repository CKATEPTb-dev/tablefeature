package ru.ckateptb.tablefeature.listener;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tablefeature.event.armor.ArmorEquipEvent;
import ru.ckateptb.tablefeature.gui.InventoryMenu;
import ru.ckateptb.tablefeature.recipe.IStrictRecipe;
import ru.ckateptb.tablefeature.temporary.*;

public class Handler implements Listener {

    @EventHandler
    public void on(AsyncPlayerChatEvent event) {
        TemporaryWaitingChatInput.get(event.getPlayer()).ifPresent(observer -> observer.onInput(event));
    }

    @EventHandler
    public void on(PlayerCommandPreprocessEvent event) {
        TemporaryWaitingChatInput.get(event.getPlayer()).ifPresent(TemporaryWaitingChatInput::end);
    }

    @EventHandler
    public void on(InventoryClickEvent event) {
        InventoryMenu.get(event.getClickedInventory()).flatMap(inventoryMenu ->
                inventoryMenu.getButton(event.getSlot())).ifPresent(inventoryButton -> inventoryButton.onClick(event));
    }

    @EventHandler
    public void on(InventoryCloseEvent event) {
        InventoryMenu.get(event.getInventory()).ifPresent(InventoryMenu::close);
    }

    @EventHandler
    public void on(BlockGrowEvent event) {
        if (TemporaryBlock.isTempBlock(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void on(BlockPhysicsEvent event) {
        Block block = event.getBlock();
        BlockFace[] relatives = new BlockFace[]{BlockFace.SELF, BlockFace.DOWN};
        for (BlockFace relative : relatives) {
            if (TemporaryBlock.isTempBlock(block.getRelative(relative).getLocation())) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void on(BlockPlaceEvent event) {
        if (TemporaryBlock.isTempBlock(event.getBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void on(BlockBreakEvent event) {
        if (TemporaryBlock.isTempBlock(event.getBlock().getLocation())) {
            event.setDropItems(false);
        }
    }

    @EventHandler
    public void on(EntityChangeBlockEvent event) {
        if (event.getEntityType() == EntityType.FALLING_BLOCK) {
            FallingBlock fallingBlock = (FallingBlock) event.getEntity();
            TemporaryFallingBlock.get(fallingBlock).ifPresent(tempFallingBlock -> {
                event.setCancelled(true);
                tempFallingBlock.end();
            });
        }
    }

    @EventHandler
    public void on(BlockFromToEvent event) {
        if (TemporaryBlock.isTempBlock(event.getBlock().getLocation())
                || TemporaryBlock.isTempBlock(event.getToBlock().getLocation())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void on(PrepareItemCraftEvent event) {
        if (event.getRecipe() == null) return;
        CraftingInventory inventory = event.getInventory();
        for (IStrictRecipe recipe : IStrictRecipe.recipes) {
            ItemStack result = recipe.getResult(inventory);
            if (result != null) {
                inventory.setResult(result);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onFallDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) entity;
            EntityDamageEvent.DamageCause cause = event.getCause();
            if (cause == EntityDamageEvent.DamageCause.FALL) {
                TemporaryFlight.get(livingEntity).ifPresent(tempFlight -> {
                    if (tempFlight.isPreventFallDamage()) event.setCancelled(true);
                });
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void on(PlayerToggleSneakEvent event) {
        TemporaryParalyze.get(event.getPlayer()).ifPresent(paralyze -> paralyze.onMoveOrSneak(event));
    }

    @EventHandler(ignoreCancelled = true)
    public void on(PlayerMoveEvent event) {
        TemporaryParalyze.get(event.getPlayer()).ifPresent(paralyze -> paralyze.onMoveOrSneak(event));
    }

    @EventHandler(ignoreCancelled = true)
    public void onArmorEquipEvent(ArmorEquipEvent event) {
        Player player = event.getPlayer();
        TemporaryArmor.get(player).ifPresent(temporaryArmor -> event.setCancelled(true));
    }

    @EventHandler(ignoreCancelled = true)
    public void on(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        if (entity != null) {
            TemporaryArmor.get(entity).ifPresent(temporaryArmor -> {
                for (ItemStack armor : entity.getEquipment().getArmorContents()) {
                    if (armor != null) event.getDrops().remove(armor);
                }
                temporaryArmor.end();
                for (ItemStack armor : entity.getEquipment().getArmorContents()) {
                    if (armor != null) event.getDrops().add(armor);
                }
            });
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void on(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        TemporaryArmor.get(player).ifPresent(Temporary::end);
        TemporaryParalyze.get(player).ifPresent(Temporary::end);
        TemporaryFlight.get(player).ifPresent(Temporary::end);
        TemporaryPotionEffect.get(player).forEach(Temporary::end);
    }
}
