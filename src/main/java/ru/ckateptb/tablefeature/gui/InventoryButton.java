package ru.ckateptb.tablefeature.gui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;


public class InventoryButton {

    private final ItemStack stack;
    public InventoryButtonHandle handle;

    public InventoryButton(ItemStack stack) {
        this.stack = stack;
        setHandle(event -> event.setCancelled(true));
    }

    public ItemStack get() {
        return stack;
    }

    public void setHandle(final InventoryButtonHandle handle) {
        this.handle = handle;
    }

    public void onClick(InventoryClickEvent event) {
        this.handle.onClick(event);
    }

    public interface InventoryButtonHandle {
        void onClick(InventoryClickEvent event);
    }
}

