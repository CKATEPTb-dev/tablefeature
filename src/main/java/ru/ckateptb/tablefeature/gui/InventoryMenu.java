package ru.ckateptb.tablefeature.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InventoryMenu implements InventoryHolder {
    private static final Map<Inventory, InventoryMenu> instances = new HashMap<>();
    private final Player player;
    private final InventoryButton[] buttons;
    public String title;
    public int rows;
    public Inventory inventory;

    public InventoryMenu(Player player, String title, int rows) {
        this.title = title;
        this.rows = rows;
        if (rows > 6 || rows < 1) {
            new IllegalArgumentException("Rows must be from 1 to 6! Set to default (6)").printStackTrace();
            this.rows = 6;
        }
        this.inventory = Bukkit.createInventory(this, this.rows * 9, this.title);
        this.buttons = new InventoryButton[this.rows * 9];
        this.player = player;
    }

    public static Optional<InventoryMenu> get(Inventory inventory) {
        return Optional.ofNullable(instances.get(inventory));
    }

    public InventoryMenu setSlot(int slot, InventoryButton button) {
        this.buttons[slot] = button;
        this.inventory.setItem(slot, button.get());
        return this;
    }

    public Optional<InventoryButton> getButton(int slot) {
        return Optional.ofNullable(this.buttons[slot]);
    }

    public void open() {
        instances.put(this.inventory, this);
        player.openInventory(this.inventory);
    }

    public void close() {
        instances.remove(this.inventory);
    }

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }

    public Player getPlayer() {
        return player;
    }
}
