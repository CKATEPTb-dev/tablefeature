package ru.ckateptb.tablefeature.temporary;

import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.tablefeature.observable.MapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.Collection;
import java.util.Optional;

public class TemporaryArmor implements Observer, Temporary {
    private static final MapObservable<LivingEntity, TemporaryArmor> instances = new MapObservable<>();

    public static Collection<TemporaryArmor> get() {
        return instances.get();
    }

    public static Optional<TemporaryArmor> get(LivingEntity livingEntity) {
        return Optional.ofNullable(instances.get(livingEntity));
    }

    public static void removeAll() {
        get().forEach(TemporaryArmor::end);
    }

    private final LivingEntity livingEntity;
    private Handler handler;
    private long duration;
    private long startTime;
    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;
    private ItemStack[] origin;

    public TemporaryArmor(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    @Override
    public TemporaryArmor withFinalHandler(Handler handler) {
        this.handler = handler;
        return this;
    }

    @Override
    public TemporaryArmor withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public TemporaryArmor withHelmet(ItemStack helmet) {
        this.helmet = helmet;
        return this;
    }

    public TemporaryArmor withChestplate(ItemStack chestplate) {
        this.chestplate = chestplate;
        return this;
    }

    public TemporaryArmor withLegging(ItemStack legging) {
        this.leggings = legging;
        return this;
    }

    public TemporaryArmor withBoot(ItemStack boot) {
        this.boots = boot;
        return this;
    }

    @Override
    public void start() {
        get(livingEntity).ifPresent(TemporaryArmor::end);
        if (duration > 0) startTime = System.currentTimeMillis();
        EntityEquipment equipment = livingEntity.getEquipment();
        this.origin = equipment.getArmorContents();
        if (helmet != null) equipment.setHelmet(helmet);
        if (chestplate != null) equipment.setChestplate(chestplate);
        if (leggings != null) equipment.setLeggings(leggings);
        if (boots != null) equipment.setBoots(boots);
        instances.addObserver(livingEntity, this);
    }

    @Override
    public void end() {
        instances.deleteObserver(this);
        livingEntity.getEquipment().setArmorContents(origin);
        if (handler != null) handler.on();
    }

    @Override
    public void update() {
        if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
    }

}