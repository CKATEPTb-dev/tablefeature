package ru.ckateptb.tablefeature.temporary;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import ru.ckateptb.tablefeature.observable.MapObservable;
import ru.ckateptb.tablefeature.observable.Observer;
import ru.ckateptb.tablefeature.utils.WorldUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class TemporaryFlight implements Observer, Temporary {
    private static final MapObservable<LivingEntity, TemporaryFlight> instances = new MapObservable<>();
    private final LivingEntity livingEntity;
    private boolean defaultFlying;
    private boolean defaultAllowFlight;
    private boolean flying;
    private boolean allowFlight;
    private long duration;
    private long startTime;
    private Handler revertHandler;
    private boolean revertOnGround;
    private boolean preventFallDamage;
    private boolean isOnGround;

    public TemporaryFlight(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    public static Collection<TemporaryFlight> get() {
        return Collections.unmodifiableCollection(instances.get());
    }

    public static Optional<TemporaryFlight> get(LivingEntity livingEntity) {
        return Optional.ofNullable(instances.get(livingEntity));
    }

    public static void removeAll() {
        get().forEach(TemporaryFlight::end);
    }

    @Override
    public TemporaryFlight withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public TemporaryFlight withFinalHandler(Handler revertHandler) {
        this.revertHandler = revertHandler;
        return this;
    }

    public TemporaryFlight withFlying(boolean flying) {
        this.flying = flying;
        return this;
    }

    public TemporaryFlight withAllowFlight(boolean allowFlight) {
        this.allowFlight = allowFlight;
        return this;
    }

    public TemporaryFlight withRevertOnGround(boolean revertOnGround) {
        this.revertOnGround = revertOnGround;
        return this;
    }

    public TemporaryFlight withPreventFallDamage(boolean preventFallDamage) {
        this.preventFallDamage = preventFallDamage;
        return this;
    }

    public boolean isPreventFallDamage() {
        return this.preventFallDamage;
    }

    @Override
    public void end() {
        instances.deleteObserver(this);
        if (livingEntity instanceof Player) {
            Player player = (Player) this.livingEntity;
            if (flying) player.setFlying(defaultFlying);
            if (allowFlight) player.setAllowFlight(defaultAllowFlight);
        }
        if (revertHandler != null) revertHandler.on();
    }

    @Override
    public void start() {
        get(livingEntity).ifPresent(TemporaryFlight::end);
        this.startTime = System.currentTimeMillis();
        if (livingEntity instanceof Player) {
            Player player = (Player) this.livingEntity;
            if (flying) {
                this.defaultFlying = player.isFlying();
                player.setFlying(true);
            }
            if (allowFlight) {
                this.defaultAllowFlight = player.getAllowFlight();
                player.setAllowFlight(true);
            }
        }
        if (revertOnGround) {
            isOnGround = WorldUtils.isOnGround(livingEntity);
        }
        instances.addObserver(livingEntity, this);
    }

    @Override
    public void update() {
        if (livingEntity == null || livingEntity.isDead()) end();
        else if (livingEntity instanceof OfflinePlayer && !((OfflinePlayer) livingEntity).isOnline()) end();
        else if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
        else if (revertOnGround) {
            if (!WorldUtils.isOnGround(livingEntity)) isOnGround = false;
            if (!isOnGround && WorldUtils.isOnGround(livingEntity)) end();
        }
    }
}
