package ru.ckateptb.tablefeature.temporary;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;
import ru.ckateptb.tablefeature.collision.Collider;
import ru.ckateptb.tablefeature.collision.Sphere;
import ru.ckateptb.tablefeature.utils.VectorUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TemporaryExplosion implements Temporary {
    private Collider.CollisionCallback collisionCallback;
    private Location location;
    private double radius;
    private long duration;
    private Handler handler;

    public TemporaryExplosion(Location location, double radius) {
        this.location = location;
        this.radius = radius;
    }

    public TemporaryExplosion withLocation(Location location) {
        this.location = location;
        return this;
    }

    public TemporaryExplosion withRadius(double radius) {
        this.radius = radius;
        return this;
    }

    public TemporaryExplosion withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public TemporaryExplosion withCollisionCallback(Collider.CollisionCallback collisionCallback) {
        this.collisionCallback = collisionCallback;
        return this;
    }

    @Override
    public TemporaryExplosion withFinalHandler(Handler revertHandler) {
        this.handler = revertHandler;
        return this;
    }

    @Override
    public void end() {
    }

    @Override
    public void start() {
        Sphere sphere = new Sphere(location, radius);
        Collection<Block> blocks = sphere.handleBlockCollisions();
        List<TemporaryFallingBlock> fallingBlocks = new ArrayList<>();
        blocks.forEach(block -> {
            Vector vector = VectorUtils.fromTo(location.clone().subtract(0, radius, 0), block.getLocation());
            fallingBlocks.add(
                    new TemporaryFallingBlock(block.getLocation(), block.getState().getData())
                            .withMaxDistance(20)
                            .withDuration(3000)
                            .withVelocity(vector.multiply(0.5))
            );
        });
        blocks.forEach(block -> new TemporaryBlock(block, Material.AIR, duration)
                .withFinalHandler(handler)
                .start());
        fallingBlocks.forEach(TemporaryFallingBlock::start);
        if (collisionCallback != null) {
            sphere.handleEntityCollisions(collisionCallback, location, true);
        }
    }

}
