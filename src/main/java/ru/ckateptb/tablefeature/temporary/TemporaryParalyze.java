package ru.ckateptb.tablefeature.temporary;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.PacketPlayOutGameStateChange;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.player.PlayerEvent;
import ru.ckateptb.tablefeature.TableFeature;
import ru.ckateptb.tablefeature.observable.MapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.Collection;
import java.util.Optional;

public class TemporaryParalyze implements Observer, Temporary {
    static {
        ProtocolLibrary.getProtocolManager().addPacketListener(
                new PacketAdapter(TableFeature.getInstance(), ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
                    public void onPacketReceiving(PacketEvent e) {
                        if (e.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                            if (e.getPlayer().getEntityId() == e.getPacket().getIntegers().read(0))
                                e.setCancelled(true);
                        }
                    }
                }
        );
    }

    private static final MapObservable<LivingEntity, TemporaryParalyze> instances = new MapObservable<>();

    public static Collection<TemporaryParalyze> get() {
        return instances.get();
    }

    public static Optional<TemporaryParalyze> get(LivingEntity livingEntity) {
        return Optional.ofNullable(instances.get(livingEntity));
    }

    public static void removeAll() {
        get().forEach(TemporaryParalyze::end);
    }

    public ArmorStand armorStand;
    private final LivingEntity livingEntity;
    private GameMode originalGameMode;
    private boolean gravity;
    protected Temporary.Handler handler;
    protected long duration;
    protected long startTime;

    public TemporaryParalyze(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    @Override
    public void start() {
        TemporaryParalyze.get(livingEntity).ifPresent(TemporaryParalyze::end);
        if (livingEntity instanceof Player) {
            Player player = ((Player) livingEntity).getPlayer();
            this.originalGameMode = player.getGameMode();
            this.armorStand = (ArmorStand) player.getWorld().spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);
            this.armorStand.setGravity(gravity);
            this.armorStand.setCanPickupItems(false);
            this.armorStand.setMarker(false);
            this.armorStand.setVisible(false);
            player.setSneaking(false);
            paralyze();
        } else {
            livingEntity.setAI(false);
        }
        if (duration > 0) startTime = System.currentTimeMillis();
        instances.addObserver(livingEntity, this);
    }

    @Override
    public void end() {
        if (livingEntity instanceof Player) {
            this.armorStand.remove();
            Player player = (Player) livingEntity;
            EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
            entityPlayer.playerConnection.sendPacket(new PacketPlayOutGameStateChange(3, originalGameMode.getValue()));
            player.setFlying(false);
            player.setSneaking(true);
            player.setSneaking(false);
        } else {
            livingEntity.setAI(true);
        }
        instances.deleteObserver(this);
        if (handler != null) handler.on();
    }

    @Override
    public void update() {
        if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
    }

    @Override
    public TemporaryParalyze withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public TemporaryParalyze withFinalHandler(Handler handler) {
        this.handler = handler;
        return this;
    }

    public void onMoveOrSneak(PlayerEvent e) {
        if (e instanceof Cancellable) {
            ((Cancellable) e).setCancelled(true);
            paralyze();
        }
    }

    public void paralyze() {
        if (livingEntity instanceof Player) {
            EntityPlayer player = ((CraftPlayer) livingEntity).getHandle();
            player.playerConnection.sendPacket(new PacketPlayOutGameStateChange(3, 3));
            player.setSpectatorTarget(((CraftEntity) armorStand).getHandle());
        }
    }

    public TemporaryParalyze withGravity(boolean gravity) {
        this.gravity = gravity;
        return this;
    }

    public ArmorStand getArmorStand() {
        return armorStand;
    }
}