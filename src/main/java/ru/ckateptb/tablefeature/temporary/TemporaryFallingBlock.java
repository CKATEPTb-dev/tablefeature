package ru.ckateptb.tablefeature.temporary;

import org.bukkit.Location;
import org.bukkit.entity.FallingBlock;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;
import ru.ckateptb.tablefeature.observable.MapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;


public class TemporaryFallingBlock implements Observer, Temporary {
    private static final MapObservable<FallingBlock, TemporaryFallingBlock> instances = new MapObservable<>();
    private Location location;
    private MaterialData materialData;
    private long duration;
    private long startTime;
    private Vector velocity;
    private FallingBlock fallingBlock;
    private double maxDistance;
    private boolean glowing;
    private boolean gravity = true;
    private Handler revertHandler;

    public TemporaryFallingBlock(Location location, MaterialData materialData) {
        this.location = location;
        this.materialData = materialData;
    }

    public static Optional<TemporaryFallingBlock> get(FallingBlock fallingBlock) {
        return Optional.ofNullable(instances.get(fallingBlock));
    }

    public static Collection<TemporaryFallingBlock> get() {
        return Collections.unmodifiableCollection(instances.get());
    }

    public static void removeAll() {
        get().forEach(TemporaryFallingBlock::end);
    }

    public TemporaryFallingBlock withLocation(Location location) {
        this.location = location;
        return this;
    }

    public TemporaryFallingBlock withMaterialData(MaterialData materialData) {
        this.materialData = materialData;
        return this;
    }

    public TemporaryFallingBlock withGravity(boolean gravity) {
        this.gravity = gravity;
        return this;
    }

    public TemporaryFallingBlock withGlowing(boolean glowing) {
        this.glowing = glowing;
        return this;
    }

    public TemporaryFallingBlock withVelocity(Vector velocity) {
        this.velocity = velocity;
        return this;
    }

    public TemporaryFallingBlock withMaxDistance(double maxDistance) {
        this.maxDistance = maxDistance;
        return this;
    }

    @Override
    public TemporaryFallingBlock withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public TemporaryFallingBlock withFinalHandler(Handler revertHandler) {
        this.revertHandler = revertHandler;
        return this;
    }

    @Override
    public void end() {
        instances.deleteObserver(this);
        this.fallingBlock.remove();
        if (this.revertHandler != null) this.revertHandler.on();
    }

    @Override
    public void start() {
        this.fallingBlock = location.getWorld().spawnFallingBlock(location, materialData);
        this.fallingBlock.setDropItem(false);
        this.fallingBlock.setHurtEntities(false);
        this.fallingBlock.setTicksLived(100);
        this.fallingBlock.setGravity(gravity);
        this.fallingBlock.setGlowing(glowing);
        if (velocity != null) {
            this.fallingBlock.setVelocity(velocity);
        }
        this.startTime = System.currentTimeMillis();
        instances.addObserver(this.fallingBlock, this);
    }

    @Override
    public void update() {
        if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
        else if (maxDistance > 0 && location.distance(this.fallingBlock.getLocation()) > maxDistance) end();
    }

    public FallingBlock getFallingBlock() {
        return fallingBlock;
    }
}
