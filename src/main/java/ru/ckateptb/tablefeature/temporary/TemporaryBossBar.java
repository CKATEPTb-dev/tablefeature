package ru.ckateptb.tablefeature.temporary;

import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import ru.ckateptb.tablefeature.observable.Observable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.List;

public class TemporaryBossBar implements Observer, Temporary {
    public static final Observable<TemporaryBossBar> instances = new Observable<>();
    private String title;
    private BarColor color;
    private BarStyle style;
    private long duration;
    private Handler handler;
    private Player[] players;
    private long startTime;
    private BossBar bossBar;

    public TemporaryBossBar(String title, BarColor color, BarStyle style) {
        this.title = title;
        this.color = color;
        this.style = style;
    }

    public static List<TemporaryBossBar> get() {
        return instances.get();
    }

    public static void removeAll() {
        get().forEach(TemporaryBossBar::end);
    }

    @Override
    public TemporaryBossBar withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public TemporaryBossBar withFinalHandler(Handler revertHandler) {
        this.handler = revertHandler;
        return this;
    }

    public TemporaryBossBar withTitle(String title) {
        this.title = title;
        return this;
    }

    public TemporaryBossBar withColor(BarColor color) {
        this.color = color;
        return this;
    }

    public TemporaryBossBar withStyle(BarStyle style) {
        this.style = style;
        return this;
    }

    public TemporaryBossBar withPlayers(Player... players) {
        this.players = players;
        return this;
    }

    @Override
    public void end() {
        instances.deleteObserver(this);
        if (this.bossBar != null) this.bossBar.removeAll();
        if (this.handler != null) handler.on();
    }

    @Override
    public void start() {
        Validate.notNull(players);
        this.startTime = System.currentTimeMillis();
        this.bossBar = Bukkit.createBossBar(title, color, style);
        this.bossBar.setProgress(1);
        for (Player player : players) {
            this.bossBar.addPlayer(player);
        }
        instances.addObserver(this);
    }

    @Override
    public void update() {
        if (duration <= 0) return;
        double spendMs = System.currentTimeMillis() - this.startTime;
        double substract = spendMs / this.duration;
        double progress = Math.max(0, 1 - substract);
        this.bossBar.setProgress(progress);
        if (progress == 0) this.end();
    }
}
