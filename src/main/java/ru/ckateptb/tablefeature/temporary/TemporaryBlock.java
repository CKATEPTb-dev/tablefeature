package ru.ckateptb.tablefeature.temporary;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import ru.ckateptb.tablefeature.block.AttachableMaterial;
import ru.ckateptb.tablefeature.observable.MultiMapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TemporaryBlock implements Observer, Temporary { //TODO PISTON BLOCK MODIFY?
    public static final BlockFace[] RELATIVES = new BlockFace[]{
            BlockFace.DOWN,
            BlockFace.EAST,
            BlockFace.NORTH,
            BlockFace.SOUTH,
            BlockFace.WEST,
            BlockFace.UP
    };

    private static final MultiMapObservable<Location, TemporaryBlock> instances = new MultiMapObservable<>();
    private final List<TemporaryBlock> attached = new ArrayList<>();
    private Block block;
    private BlockState original;
    private BlockState tempState;
    private Location location;
    private Material type;
    private boolean applyPhysics = false;
    private byte data;
    protected Temporary.Handler handler;
    protected long duration;
    protected long startTime;

    public TemporaryBlock(Block block, Material type, long duration) {
        if (!type.isBlock()) return;
        this.block = block;
        this.type = type;
        this.original = block.getState();
        this.tempState = block.getState();
        this.location = block.getLocation();
        this.duration = duration;
    }

    public static Collection<TemporaryBlock> get() {
        return instances.get();
    }

    public static Collection<TemporaryBlock> get(Location location) {
        return instances.getCollection(location);
    }

    public static void removeAll() {
        get().forEach(TemporaryBlock::revertOriginal);
    }

    public static boolean isTempBlock(Location location) {
        return !get(location).isEmpty();
    }

    public TemporaryBlock withMaterial(Material type) {
        this.type = type;
        return this;
    }

    public TemporaryBlock withPhysics(boolean applyPhysics) {
        this.applyPhysics = applyPhysics;
        return this;
    }

    @Override
    public TemporaryBlock withFinalHandler(Handler revertHandler) {
        this.handler = revertHandler;
        return this;
    }

    public TemporaryBlock withData(byte data) {
        this.data = data;
        return this;
    }

    public TemporaryBlock addDuration(long duration) {
        attached.forEach(attach -> attach.addDuration(duration));
        this.duration += duration;
        return this;
    }

    @Override
    public void start() {
        this.startTime = System.currentTimeMillis();
        this.original = block.getState();
        get(this.location).forEach(tempBlock -> {
            tempBlock.addDuration(this.duration);
            this.original = tempBlock.original;
        });
        this.tempState = block.getState();
        if (tempState instanceof Container) {
            ((Container) tempState).getInventory().clear();
        }
        this.block.setType(type, applyPhysics);
        this.block.setData(data, applyPhysics);
        if (duration > 0) startTime = System.currentTimeMillis();
        instances.addObserver(location, this);
        for (BlockFace face : RELATIVES) {
            Block relativeBlock = block.getRelative(face);
            if (AttachableMaterial.isAttach(relativeBlock, block)) {
                TemporaryBlock relativeTempBlock = new TemporaryBlock(relativeBlock, Material.AIR, duration);
                relativeTempBlock.start();
                attached.add(relativeTempBlock);
            }
        }
    }

    @Override
    public void end() {
        tempState.update(true, applyPhysics);
        instances.deleteObserver(location, this);
        if (handler != null) handler.on();
    }


    @Override
    public void update() {
        if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
    }

    @Override
    public Temporary withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public void revertOriginal() {
        tempState = original;
        end();
    }
}