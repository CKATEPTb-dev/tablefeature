package ru.ckateptb.tablefeature.temporary;

public interface Temporary {

    Temporary withDuration(long duration);

    Temporary withFinalHandler(Handler handler);

    void end();

    void start();

    interface Handler {
        void on();
    }
}
