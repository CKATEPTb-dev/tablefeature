package ru.ckateptb.tablefeature.temporary;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.ckateptb.tablefeature.observable.MultiMapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.Collection;

public class TemporaryPotionEffect implements Observer, Temporary {
    private static final MultiMapObservable<LivingEntity, TemporaryPotionEffect> instances = new MultiMapObservable<>();

    public static Collection<TemporaryPotionEffect> get() {
        return instances.get();
    }

    public static Collection<TemporaryPotionEffect> get(LivingEntity livingEntity) {
        return instances.getCollection(livingEntity);
    }

    public static void removeAll() {
        get().forEach(TemporaryPotionEffect::end);
    }

    private final LivingEntity livingEntity;
    private Handler handler;
    private long duration;
    private long startTime;
    private int power = 1;
    private PotionEffectType type;
    private PotionEffect effect;

    public TemporaryPotionEffect(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
    }

    @Override
    public TemporaryPotionEffect withFinalHandler(Handler handler) {
        this.handler = handler;
        return this;
    }

    @Override
    public TemporaryPotionEffect withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public TemporaryPotionEffect withType(PotionEffectType type) {
        this.type = type;
        return this;
    }

    public TemporaryPotionEffect withPower(int power) {
        this.power = power;
        return this;
    }

    @Override
    public void start() {
        if (duration > 0) startTime = System.currentTimeMillis();
        livingEntity.removePotionEffect(type);
        effect = new PotionEffect(type, Integer.MAX_VALUE, power - 1, false, false);
        instances.addObserver(livingEntity, this);
    }

    @Override
    public void end() {
        instances.deleteObserver(livingEntity, this);
        livingEntity.removePotionEffect(type);
        if (handler != null) handler.on();
    }

    @Override
    public void update() {
        if (duration > 0 && System.currentTimeMillis() - startTime > duration) end();
        else {
            livingEntity.addPotionEffect(effect);
        }
    }
}