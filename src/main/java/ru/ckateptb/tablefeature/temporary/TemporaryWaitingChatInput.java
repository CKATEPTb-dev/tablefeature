package ru.ckateptb.tablefeature.temporary;

import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import ru.ckateptb.tablefeature.observable.MapObservable;
import ru.ckateptb.tablefeature.observable.Observer;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class TemporaryWaitingChatInput implements Observer, Temporary {
    private static final MapObservable<Player, TemporaryWaitingChatInput> instances = new MapObservable<>();
    private final Player player;
    private Handler handler;
    private long duration;
    private TemporaryBossBar bossBar;
    private long startTime;

    public TemporaryWaitingChatInput(Player player, Handler handler) {
        this.player = player;
        this.handler = handler;
    }

    public static Collection<TemporaryWaitingChatInput> get() {
        return Collections.unmodifiableCollection(instances.get());
    }

    public static Optional<TemporaryWaitingChatInput> get(Player player) {
        return Optional.ofNullable(instances.get(player));
    }

    public static void removeAll() {
        get().forEach(TemporaryWaitingChatInput::end);
    }

    @Override
    public TemporaryWaitingChatInput withDuration(long duration) {
        this.duration = duration;
        return this;
    }

    @Override
    public TemporaryWaitingChatInput withFinalHandler(Temporary.Handler handler) {
        this.withInputHandler(event -> {
            handler.on();
            return true;
        });
        return this;
    }

    public TemporaryWaitingChatInput withInputHandler(Handler handler) {
        this.handler = handler;
        return this;
    }

    public TemporaryWaitingChatInput withBossBar(String title, BarColor color, BarStyle style) {
        this.bossBar = new TemporaryBossBar(title, color, style)
                .withFinalHandler(() -> instances.deleteObserver(this))
                .withPlayers(player);
        return this;
    }

    @Override
    public void end() {
        if (bossBar != null) {
            bossBar.end();
        } else {
            instances.deleteObserver(this);
        }
    }

    @Override
    public void start() {
        get(player).ifPresent(TemporaryWaitingChatInput::end);
        if (bossBar != null) {
            this.bossBar.withDuration(duration).start();
        } else {
            this.startTime = System.currentTimeMillis();
        }
        instances.addObserver(player, this);
    }

    @Override
    public void update() {
        if (bossBar == null && this.duration > 0) {
            if (System.currentTimeMillis() - startTime >= duration) end();
        }
    }

    public boolean onInput(AsyncPlayerChatEvent event) {
        if (handler.on(event)) {
            end();
            return true;
        }
        return false;
    }

    public interface Handler {
        boolean on(AsyncPlayerChatEvent event);
    }

}
