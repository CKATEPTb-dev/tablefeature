package ru.ckateptb.tablefeature.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.material.Attachable;
import org.bukkit.material.Directional;
import org.bukkit.material.MaterialData;

public enum AttachableMaterial {
    SAPLING(Material.SAPLING, BlockFace.DOWN),
    TALL_GRASS(Material.LONG_GRASS, BlockFace.DOWN),
    DEAD_BUSH(Material.DEAD_BUSH, BlockFace.DOWN),
    YELLOW_FLOWER(Material.YELLOW_FLOWER, BlockFace.DOWN),
    RED_ROSE(Material.RED_ROSE, BlockFace.DOWN),
    BROWN_MUSHROOM(Material.BROWN_MUSHROOM, BlockFace.DOWN),
    RED_MUSHROOM(Material.RED_MUSHROOM, BlockFace.DOWN),
    TORCH(Material.TORCH, BlockFace.DOWN, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    FIRE(Material.FIRE, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    CROPS(Material.CROPS, BlockFace.DOWN),
    SIGN_POST(Material.SIGN_POST, BlockFace.DOWN),
    WALL_SIGN(Material.WALL_SIGN, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    LEVER(Material.LEVER, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    STONE_PLATE(Material.STONE_PLATE, BlockFace.DOWN),
    IRON_DOOR_BLOCK(Material.IRON_DOOR_BLOCK, BlockFace.DOWN),
    WOOD_PLATE(Material.WOOD_PLATE, BlockFace.DOWN),
    REDSTONE_TORCH_OFF(Material.REDSTONE_TORCH_OFF, BlockFace.DOWN, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    REDSTONE_TORCH_ON(Material.REDSTONE_TORCH_ON, BlockFace.DOWN, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    STONE_BUTTON(Material.STONE_BUTTON, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    SNOW(Material.SNOW, BlockFace.DOWN),
    CACTUS(Material.CACTUS, BlockFace.DOWN),
    SUGAR_CANE_BLOCK(Material.SUGAR_CANE_BLOCK, BlockFace.DOWN),
    PORTAL(Material.PORTAL, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    CAKE_BLOCK(Material.CAKE_BLOCK, BlockFace.DOWN),
    DIODE_BLOCK_OFF(Material.DIODE_BLOCK_OFF, BlockFace.DOWN),
    DIODE_BLOCK_ON(Material.DIODE_BLOCK_ON, BlockFace.DOWN),
    PUMPKIN_STEM(Material.PUMPKIN_STEM, BlockFace.DOWN),
    MELON_STEM(Material.MELON_STEM, BlockFace.DOWN),
    VINE(Material.VINE, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    WATER_LILY(Material.WATER_LILY, BlockFace.DOWN),
    NETHER_WARTS(Material.NETHER_WARTS, BlockFace.DOWN),
    ENDER_PORTAL(Material.ENDER_PORTAL, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    COCOA(Material.COCOA, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    TRIPWIRE_HOOK(Material.TRIPWIRE_HOOK, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    FLOWER_POT(Material.FLOWER_POT, BlockFace.DOWN),
    CARROT(Material.CARROT, BlockFace.DOWN),
    POTATO(Material.POTATO, BlockFace.DOWN),
    WOOD_BUTTON(Material.WOOD_BUTTON, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    GOLD_PLATE(Material.GOLD_PLATE, BlockFace.DOWN),
    IRON_PLATE(Material.IRON_PLATE, BlockFace.DOWN),
    REDSTONE_COMPARATOR_OFF(Material.REDSTONE_COMPARATOR_OFF, BlockFace.DOWN),
    REDSTONE_COMPARATOR_ON(Material.REDSTONE_COMPARATOR_ON, BlockFace.DOWN),
    ACTIVATOR_RAIL(Material.ACTIVATOR_RAIL, BlockFace.DOWN),
    CARPET(Material.CARPET, BlockFace.DOWN),
    DOUBLE_PLANT(Material.DOUBLE_PLANT, BlockFace.DOWN),
    STANDING_BANNER(Material.STANDING_BANNER, BlockFace.DOWN),
    WALL_BANNER(Material.WALL_BANNER, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    SPRUCE_DOOR(Material.SPRUCE_DOOR, BlockFace.DOWN),
    BIRCH_DOOR(Material.BIRCH_DOOR, BlockFace.DOWN),
    JUNGLE_DOOR(Material.JUNGLE_DOOR, BlockFace.DOWN),
    ACACIA_DOOR(Material.ACACIA_DOOR, BlockFace.DOWN),
    DARK_OAK_DOOR(Material.DARK_OAK_DOOR, BlockFace.DOWN),
    CHORUS_PLANT(Material.CHORUS_PLANT, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    CHORUS_FLOWER(Material.CHORUS_FLOWER, BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST),
    BEETROOT_BLOCK(Material.BEETROOT_BLOCK, BlockFace.DOWN);


    private final Material material;
    private final BlockFace[] faces;

    AttachableMaterial(Material material, BlockFace... faces) {
        this.material = material;
        this.faces = faces;
    }

    public static AttachableMaterial get(Material material) {
        for (AttachableMaterial attachableMaterial : AttachableMaterial.values()) {
            if (attachableMaterial.material == material) return attachableMaterial;
        }
        return null;
    }

    public static boolean isAttach(Block from, Block to) {
        MaterialData data = from.getState().getData();
        if (data instanceof Attachable) {
            if (from.getRelative(((Attachable) data).getAttachedFace()).equals(to)) {
                return true;
            }
        }
        AttachableMaterial material = AttachableMaterial.get(from.getType());
        if (material != null) {
            if (data instanceof Directional) {
                return from.getRelative(((Directional) data).getFacing().getOppositeFace()).equals(to);
            }
            for (BlockFace face : material.getFaces()) {
                if (from.getRelative(face).equals(to)) return true;
            }
        }
        return false;
    }

    public Material getMaterial() {
        return material;
    }

    public BlockFace[] getFaces() {
        return faces;
    }
}
