package ru.ckateptb.tablefeature.utils;

import java.lang.reflect.Array;

public class ArrayUtils {
    public static Object[] concatenate(Object[] A, Object[] B) {
        int aLen = A.length, bLen = B.length;
        Object[] C = (Object[]) Array.newInstance(A.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(A, 0, C, 0, aLen);
        System.arraycopy(B, 0, C, aLen, bLen);
        return C;
    }
}
