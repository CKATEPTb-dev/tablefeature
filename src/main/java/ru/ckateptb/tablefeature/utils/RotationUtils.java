package ru.ckateptb.tablefeature.utils;

import net.minecraft.server.v1_12_R1.PacketPlayOutEntity;
import org.bukkit.entity.Entity;

public class RotationUtils {
    public static PacketPlayOutEntity.PacketPlayOutEntityLook create(Entity entity, float yaw, float pitch) {
        return new PacketPlayOutEntity.PacketPlayOutEntityLook(entity.getEntityId(),
                (byte) yaw,
                (byte) pitch,
                false);
    }
}
