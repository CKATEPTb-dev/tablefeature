package ru.ckateptb.tablefeature.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeUtils {
    public static String formatTime(long time) {
        StringBuilder stringDate = new StringBuilder();

        long days = TimeUnit.MILLISECONDS.toDays(time);
        time -= TimeUnit.DAYS.toMillis(days);
        if (days > 0) stringDate.append(days).append(" ");

        long hours = TimeUnit.MILLISECONDS.toHours(time);
        time -= TimeUnit.HOURS.toMillis(hours);
        if (hours > 0) stringDate.append(hours).append(" ");

        long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
        time -= TimeUnit.MINUTES.toMillis(minutes);
        if (minutes > 0) stringDate.append(minutes).append(" ");

        long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
        if (seconds > 0) stringDate.append(seconds).append(" ");
        return stringDate.toString();
    }

    public static String getDate(String format, long time) {
        /*"Последний раз был в сети dd.MM.yyyy в HH:mm:ss"*/
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(new Date(time));
    }
}
