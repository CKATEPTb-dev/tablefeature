package ru.ckateptb.tablefeature.utils;

public class AngelUtils {
    public static float adapt360to180(float angel) {
        if(angel < 0) angel *= -1;
        if(angel > 360) angel %= 360;
        if(angel > 180) angel -= 360;
        return angel;
    }
    public static float adapt180to360(float angel) {
        if(angel < 0)  {
            angel *= -1;
            angel += 180;
        }
        if(angel > 360) angel %= 360;
        return angel;
    }
}
