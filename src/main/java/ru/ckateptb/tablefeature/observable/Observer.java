package ru.ckateptb.tablefeature.observable;

public interface Observer {
    default void update() {
    }
}
