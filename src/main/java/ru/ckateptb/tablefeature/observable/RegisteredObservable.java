package ru.ckateptb.tablefeature.observable;

import org.bukkit.Bukkit;
import ru.ckateptb.tablefeature.TableFeature;

public abstract class RegisteredObservable<T extends Observer> implements IObservable<T> {
    public RegisteredObservable() {
        this.register();
    }

    private void register() {
        Bukkit.getScheduler().runTaskTimer(TableFeature.getInstance(), this::update, 0, 0);
    }
}