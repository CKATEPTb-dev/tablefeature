package ru.ckateptb.tablefeature.recipe;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.Plugin;
import ru.ckateptb.tablefeature.utils.ArrayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RecipeBuilder {
    static final char[] args = new char[]{'Q', 'W', 'E', 'A', 'S', 'D', 'Z', 'X', 'C'};
    public final NamespacedKey key;
    public final ItemStack result;
    private ItemStack[] firstLine = new ItemStack[3];
    private ItemStack[] secondLine = new ItemStack[3];
    private ItemStack[] thirdLine = new ItemStack[3];

    public RecipeBuilder(Plugin plugin, ItemStack result) {
        this.key = new NamespacedKey(
                plugin,
                plugin.getName().toLowerCase().replaceAll(" ", "_") + "_" + UUID.randomUUID().toString()
        );
        this.result = result;
    }

    public RecipeBuilder withFirstLine(ItemStack first, ItemStack second, ItemStack third) {
        this.firstLine = new ItemStack[]{first, second, third};
        return this;
    }

    public RecipeBuilder withSecondLine(ItemStack first, ItemStack second, ItemStack third) {
        this.secondLine = new ItemStack[]{first, second, third};
        return this;
    }

    public RecipeBuilder withThirdLine(ItemStack first, ItemStack second, ItemStack third) {
        this.thirdLine = new ItemStack[]{first, second, third};
        return this;
    }

    public ShapedRecipe buildShaped() {
        ShapedRecipe recipe = new ShapedRecipe(key, result);
        StringBuilder shapeBuilder = new StringBuilder();
        ItemStack[] lines = (ItemStack[]) ArrayUtils.concatenate(ArrayUtils.concatenate(firstLine, secondLine), thirdLine);
        List<IngredientQueue> list = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            ItemStack stack = lines[i];
            if (stack == null || stack.getType() == Material.AIR) {
                shapeBuilder.append(" ");
            } else {
                shapeBuilder.append(args[i]);
                int finalI = i;
                list.add(() -> recipe.setIngredient(args[finalI], lines[finalI].getData()));
            }
        }
        String shape = shapeBuilder.toString();
        String firstShape = shape.substring(0, 3);
        String secondShape = shape.substring(3, 6);
        String third = shape.substring(6, 9);
        recipe.shape(firstShape, secondShape, third);
        list.forEach(IngredientQueue::handle);
        Bukkit.getServer().addRecipe(recipe);
        return recipe;
    }

    public ShapelessRecipe buildShapeless() {
        ShapelessRecipe recipe = new ShapelessRecipe(key, result);
        ItemStack[] lines = (ItemStack[]) ArrayUtils.concatenate(ArrayUtils.concatenate(firstLine, secondLine), thirdLine);
        for (ItemStack stack : lines) {
            if (stack != null && stack.getType() != Material.AIR) {
                recipe.addIngredient(stack.getAmount(), stack.getData());
            }
        }
        Bukkit.getServer().addRecipe(recipe);
        return recipe;
    }

    public StrictShapelessRecipe buildStrictShapeless() {
        StrictShapelessRecipe recipe = new StrictShapelessRecipe(key, new ItemStack(Material.AIR));
        recipe.setOutput(result);
        ItemStack[] lines = (ItemStack[]) ArrayUtils.concatenate(ArrayUtils.concatenate(firstLine, secondLine), thirdLine);
        for (ItemStack stack : lines) {
            if (stack != null && stack.getType() != Material.AIR) {
                recipe.addIngredient(stack);
            }
        }
        Bukkit.getServer().addRecipe(recipe);
        IStrictRecipe.recipes.add(recipe);
        return recipe;
    }

    public StrictShapedRecipe buildStrictShaped() {
        StrictShapedRecipe recipe = new StrictShapedRecipe(key, new ItemStack(Material.AIR));
        recipe.setOutput(result);
        StringBuilder shapeBuilder = new StringBuilder();
        ItemStack[] lines = (ItemStack[]) ArrayUtils.concatenate(ArrayUtils.concatenate(firstLine, secondLine), thirdLine);
        for (int i = 0; i < lines.length; i++) {
            ItemStack stack = lines[i];
            if (stack == null || stack.getType() == Material.AIR) {
                shapeBuilder.append(" ");
            } else {
                shapeBuilder.append(args[i]);
                recipe.setIngredient(args[i], lines[i]);
            }
        }
        String shape = shapeBuilder.toString();
        String firstShape = shape.substring(0, 3);
        String secondShape = shape.substring(3, 6);
        String third = shape.substring(6, 9);
        recipe.shape(firstShape, secondShape, third);
        Bukkit.getServer().addRecipe(recipe);
        IStrictRecipe.recipes.add(recipe);
        return recipe;
    }

    private interface IngredientQueue {
        void handle();
    }

}
