package ru.ckateptb.tablefeature.recipe;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public interface IStrictRecipe {
    List<IStrictRecipe> recipes = new ArrayList<>();

    ItemStack[] getIngredients();

    NamespacedKey getKey();

    ItemStack getResult(CraftingInventory inventory);

    ItemStack getOutput();

    //TODO CRUTCH
    void setOutput(ItemStack output);

}
