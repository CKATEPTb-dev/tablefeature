package ru.ckateptb.tablefeature.recipe;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.HashMap;
import java.util.Map;

public class StrictShapedRecipe extends ShapedRecipe implements IStrictRecipe {
    private final Map<Character, ItemStack> ingredients = new HashMap<>();
    private ItemStack realOutput;

    /**
     * @see RecipeBuilder#buildStrictShaped()
     */
    StrictShapedRecipe(NamespacedKey key, ItemStack result) {
        super(key, result);
    }

    public void setIngredient(char key, ItemStack itemStack) {
        this.ingredients.put(key, itemStack);
    }

    @Override
    public Map<Character, ItemStack> getIngredientMap() {
        HashMap<Character, ItemStack> result = new HashMap<>();
        for (Map.Entry<Character, ItemStack> characterItemStackEntry : this.ingredients.entrySet()) {
            if (characterItemStackEntry.getValue() == null) {
                result.put(characterItemStackEntry.getKey(), null);
            } else {
                result.put(characterItemStackEntry.getKey(), characterItemStackEntry.getValue().clone());
            }
        }
        return result;
    }

    @Override
    public ItemStack[] getIngredients() {
        return getIngredientMap().values().toArray(new ItemStack[0]);
    }

    @Override
    public ItemStack getResult(CraftingInventory inventory) {
        for (int i = 1; i < inventory.getSize(); i++) {
            ItemStack original = ingredients.get(RecipeBuilder.args[i - 1]);
            if (original == null) {
                continue;
            }
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack == null) {
                return null;
            }
            if (itemStack.getAmount() < original.getAmount()) {
                return null;
            }
            ItemStack cloneOriginal = original.clone();
            ItemStack cloneItemStack = itemStack.clone();
            cloneOriginal.setAmount(1);
            cloneItemStack.setAmount(1);
            if (!cloneOriginal.equals(cloneItemStack)) {
                return null;
            }
        }
        return getOutput();
    }

    @Override
    public ItemStack getOutput() {
        return this.realOutput;
    }

    @Override
    public void setOutput(ItemStack output) {
        this.realOutput = output;
    }
}
