package ru.ckateptb.tablefeature.recipe;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

import java.util.ArrayList;
import java.util.List;

public class StrictShapelessRecipe extends ShapelessRecipe implements IStrictRecipe {
    private final List<ItemStack> ingredients = new ArrayList<>();
    private ItemStack realOutput;

    /**
     * @see RecipeBuilder#buildStrictShapeless
     */
    StrictShapelessRecipe(NamespacedKey key, ItemStack result) {
        super(key, result);
    }

    public void addIngredient(ItemStack itemStack) {
        this.ingredients.add(itemStack);
    }

    @Override
    public List<ItemStack> getIngredientList() {
        ArrayList<ItemStack> result = new ArrayList<>(this.ingredients.size());
        for (ItemStack ingredient : this.ingredients) {
            result.add(ingredient.clone());
        }
        return result;
    }

    @Override
    public ItemStack[] getIngredients() {
        return ingredients.toArray(new ItemStack[0]);
    }

    @Override
    public ItemStack getResult(CraftingInventory inventory) {
        for (ItemStack original : ingredients) {
            if (!inventory.containsAtLeast(original, original.getAmount())) {
                return null;
            }
        }
        return getOutput();
    }

    @Override
    public ItemStack getOutput() {
        return this.realOutput;
    }

    @Override
    public void setOutput(ItemStack output) {
        this.realOutput = output;
    }
}
