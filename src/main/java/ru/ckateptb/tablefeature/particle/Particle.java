package ru.ckateptb.tablefeature.particle;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.Validate;
import org.bukkit.Location;
import org.bukkit.material.MaterialData;
import ru.ckateptb.tablefeature.utils.AdaptUtils;

public enum Particle {
    EXPLOSION_NORMAL,
    EXPLOSION_LARGE,
    EXPLOSION_HUGE,
    FIREWORKS_SPARK,
    WATER_BUBBLE,
    WATER_SPLASH,
    WATER_WAKE,
    SUSPENDED,
    SUSPENDED_DEPTH,
    CRIT,
    CRIT_MAGIC,
    SMOKE_NORMAL,
    SMOKE_LARGE,
    SPELL,
    SPELL_INSTANT,
    SPELL_MOB,
    SPELL_MOB_AMBIENT,
    SPELL_WITCH,
    DRIP_WATER,
    DRIP_LAVA,
    VILLAGER_ANGRY,
    VILLAGER_HAPPY,
    TOWN_AURA,
    NOTE,
    PORTAL,
    ENCHANTMENT_TABLE,
    FLAME,
    LAVA,
    FOOTSTEP,
    CLOUD,
    REDSTONE,
    SNOWBALL,
    SNOW_SHOVEL,
    SLIME,
    HEART,
    BARRIER,
    ITEM_CRACK,
    BLOCK_CRACK,
    BLOCK_DUST,
    WATER_DROP,
    ITEM_TAKE,
    MOB_APPEARANCE,
    DRAGON_BREATH,
    END_ROD,
    DAMAGE_INDICATOR,
    SWEEP_ATTACK,
    FALLING_DUST,
    TOTEM,
    SPIT;

    private final org.bukkit.Particle original;

    Particle() {
        this.original = org.bukkit.Particle.valueOf(name());
    }

    public void displayColoredParticle(final Location loc, final String hexColor, final float xOffset, final float yOffset, final float zOffset, final int amount, final float brightness) {
        Validate.isTrue(this == REDSTONE || this == SPELL_MOB || this == SPELL_MOB_AMBIENT,
                "As of version 1.12.2, there are 3 types of particles that are colorizeable: REDSTONE, SPELL_MOB and SPELL_MOB_AMBIENT.");
        int[] rgb = AdaptUtils.toRGB(hexColor);
        for (int i = 0; i < amount; i++) {
            double x = RandomUtils.nextDouble(0, xOffset / 2);
            x = RandomUtils.nextBoolean() ? x : -x;
            double y = RandomUtils.nextDouble(0, yOffset / 2);
            y = RandomUtils.nextBoolean() ? y : -y;
            double z = RandomUtils.nextDouble(0, zOffset / 2);
            z = RandomUtils.nextBoolean() ? z : -z;
            loc.add(x, y, z);
            display(loc, rgb[0] / 255F, rgb[1] / 255F, rgb[2] / 255F, brightness, 0);
        }
    }

    public void display(final Location center, final float offsetX, final float offsetY, final float offsetZ, final float speed, final int amount) {
        center.getWorld().spawnParticle(original, center, amount, offsetX, offsetY, offsetZ, speed);
    }

    public void display(final Location center, final float offsetX, final float offsetY, final float offsetZ, final float speed, final int amount, MaterialData data) {
        center.getWorld().spawnParticle(original, center, amount, offsetX, offsetY, offsetZ, speed, data);
    }
}
