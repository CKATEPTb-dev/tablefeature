package ru.ckateptb.tablefeature.command;

import org.bukkit.command.CommandSender;
import ru.ckateptb.tablefeature.TableFeature;

import java.util.Collections;
import java.util.List;

public class TableFeatureCommand extends ForceCommand {
    public TableFeatureCommand() {
        super("tablefeature", "tf");
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        if (sender.hasPermission("tablefeature.reload")) {
            TableFeature.getInstance().removeAllTemporary();
            return true;
        }
        return false;
    }

    @Override
    public List<String> tab(CommandSender sender, String[] args, List<String> list) {
        return Collections.singletonList("reload");
    }
}
