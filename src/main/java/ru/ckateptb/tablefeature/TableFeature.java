package ru.ckateptb.tablefeature;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.tablefeature.command.TableFeatureCommand;
import ru.ckateptb.tablefeature.event.armor.ArmorListener;
import ru.ckateptb.tablefeature.listener.Handler;
import ru.ckateptb.tablefeature.temporary.*;

import java.util.Collections;

public final class TableFeature extends JavaPlugin {
    private static TableFeature instance;

    public static TableFeature getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        new TableFeatureCommand();
        PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new Handler(), this);
        pluginManager.registerEvents(new ArmorListener(Collections.emptyList()), this);
        removeAllTemporary();
    }

    @Override
    public void onDisable() {
        removeAllTemporary();
    }

    public void removeAllTemporary() {
        TemporaryArmor.removeAll();
        TemporaryBlock.removeAll();
        TemporaryBossBar.removeAll();
        TemporaryFallingBlock.removeAll();
        TemporaryFlight.removeAll();
        TemporaryWaitingChatInput.removeAll();
        TemporaryPotionEffect.removeAll();
        TemporaryParalyze.removeAll();
    }
}