package ru.ckateptb.tablefeature.builders;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.*;
import org.bukkit.block.banner.Pattern;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import java.lang.reflect.Field;
import java.util.*;

public class ItemBuilder {
    private final Material material;
    private int amount = 1;
    private short durability;
    private String displayName;
    private List<String> lore = new ArrayList<>();
    private boolean unbreakable = false;
    private List<ItemFlag> flags = new ArrayList<>();
    private Map<Enchantment, Integer> enchantmentIntegerMap = new HashMap<>();
    private UUID owner;
    private String texture;
    private String signature = "H116D5fhmj/7BVWqiRQilXmvoJO6wJzXH4Dvou6P2o9YMb+HaJT8s9+zt03GMYTipzK+NsW2D" +
            "2JfzagnxLUTuiOtrCHm6V2udOM0HG0JeL4zR0Wn5oHmu+S7kUPUbt7HVlKaRXry5bobFQ06nUf7hOV3kPfpUJsfMajfabmoJ9RGMRVot" +
            "3uQszjKOHQjxyAjfHP2rjeI/SktBrSscx0DzwBW9LCra7g/6Cp7/xPQTIZsqz2Otgp6i2h3YpXJPy02j4pIk0H4biR3CaU7FB0V4/D1H" +
            "vjd08giRvUpqF0a1w9rbpIWIH5GTUP8eLFdG/9SnHqMCQrTj4KkQiN0GdBO18JvJS/40LTn3ZLag5LBIa7AyyGus27N3wdIccvToQ6kH" +
            "HRVpW7cUSXjircg3LOsSQbJmfLoVJ/KAF/m+de4PxIjOJIcbiOkVyQfMQltPg26VzRiu3F0qRvJNAAydH8AHdaqhkpSf6yjHqPU3p3BH" +
            "FJld5o59WoD4WNkE3wOC//aTpV/f9RJ0JQko08v2mGBVKx7tpN7vHD1qD5ILzV1nDCV1/qbKgiOK9QmdXqZw9J3pM/DHtZ6eiRKni9Bu" +
            "GWlbWFN/qfFO2xY+J7SYFqTxBbffmvwvuF83QP5UdRTNVLYoV5S+yR5ac7fVWUZmLbq7tawyuCu0Dw24M9E1BSnpSc=";
    private String[] pages;
    private String author;
    private FireworkEffect fireworkEffect;
    private Color color;
    private boolean scaling;
    private Set<PotionEffect> potionEffects = new HashSet<>();
    private List<Pattern> patterns = new ArrayList<>();

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder withAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder withDurability(int durability) {
        this.durability = (short) durability;
        return this;
    }

    public ItemBuilder withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public ItemBuilder withLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder addLore(String... lore) {
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public ItemBuilder withUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public ItemBuilder withFlags(List<ItemFlag> flags) {
        this.flags = flags;
        return this;
    }

    public ItemBuilder addFlags(ItemFlag... flags) {
        this.flags.addAll(Arrays.asList(flags));
        return this;
    }

    public ItemBuilder withEnchantment(Map<Enchantment, Integer> map) {
        this.enchantmentIntegerMap = map;
        return this;
    }

    public ItemBuilder withEnchantment(Enchantment enchantment, Integer integer) {
        this.enchantmentIntegerMap.put(enchantment, integer);
        return this;
    }

    public ItemBuilder withOwner(UUID owner) {
        this.owner = owner;
        return this;
    }

    public ItemBuilder withOwner(OfflinePlayer owner) {
        return withOwner(owner.getUniqueId());
    }

    public ItemBuilder withTexture(String texture) {
        this.texture = texture;
        return this;
    }

    public ItemBuilder withSignature(String signature) {
        this.signature = signature;
        return this;
    }

    public ItemBuilder withPages(String[] pages) {
        this.pages = pages;
        return this;
    }

    public ItemBuilder withAuthor(String author) {
        this.author = author;
        return this;
    }

    public ItemBuilder withFireworkEffect(FireworkEffect effect) {
        this.fireworkEffect = effect;
        return this;
    }

    public ItemBuilder withColor(Color color) {
        this.color = color;
        return this;
    }

    public ItemBuilder withScaling(boolean scaling) {
        this.scaling = scaling;
        return this;
    }

    public ItemBuilder withPotionEffects(Set<PotionEffect> effects) {
        this.potionEffects = effects;
        return this;
    }

    public ItemBuilder addPotionEffects(PotionEffect... effect) {
        this.potionEffects.addAll(Arrays.asList(effect));
        return this;
    }

    public ItemBuilder withPatterns(List<Pattern> patterns) {
        this.patterns = patterns;
        return this;
    }

    public ItemBuilder addPatterns(Pattern... patterns) {
        this.patterns.addAll(Arrays.asList(patterns));
        return this;
    }

    public ItemStack build() {
        ItemStack stack = new ItemStack(material);
        if (material == Material.AIR) return stack;
        ItemMeta itemMeta = Bukkit.getItemFactory().getItemMeta(material);
        if (itemMeta instanceof BannerMeta) {
            ((BannerMeta) itemMeta).setPatterns(this.patterns);
        } else if (itemMeta instanceof BookMeta) {
            if (author != null) ((BookMeta) itemMeta).setAuthor(author);
            if (pages != null) ((BookMeta) itemMeta).setPages(pages);
            if (displayName != null) ((BookMeta) itemMeta).setTitle(displayName);
        } else if (itemMeta instanceof SkullMeta) {
            if (owner != null) {
                ((SkullMeta) itemMeta).setOwningPlayer(Bukkit.getOfflinePlayer(owner));
            }
            if (texture != null) {
                GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
                gameProfile.getProperties().put("textures", new Property("textures", texture, signature));
                try {
                    Field profileField = ((SkullMeta) itemMeta).getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(itemMeta, gameProfile);
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException exception) {
                    exception.printStackTrace();
                }
            }
        } else if (itemMeta instanceof PotionMeta) {
            ((PotionMeta) itemMeta).setBasePotionData(new PotionData(PotionType.UNCRAFTABLE));
            ((PotionMeta) itemMeta).clearCustomEffects();
            if (potionEffects.size() > 0) ((PotionMeta) itemMeta).setColor(Color.fromRGB(3694535));
            for (PotionEffect potionEffect : potionEffects) {
                ((PotionMeta) itemMeta).addCustomEffect(potionEffect, true);
            }
        } else if (itemMeta instanceof EnchantmentStorageMeta) {
            for (Map.Entry<Enchantment, Integer> entry : enchantmentIntegerMap.entrySet()) {
                ((EnchantmentStorageMeta) itemMeta).addStoredEnchant(entry.getKey(), entry.getValue(), true);
            }
        } else if (itemMeta instanceof FireworkEffectMeta) {
            ((FireworkEffectMeta) itemMeta).setEffect(fireworkEffect);
        } else if (itemMeta instanceof LeatherArmorMeta) {
            ((LeatherArmorMeta) itemMeta).setColor(color);
        } else if (itemMeta instanceof MapMeta) {
            ((MapMeta) itemMeta).setScaling(scaling);
        }
        stack.setAmount(this.amount);
        stack.setDurability(this.durability);
        if (this.displayName != null) itemMeta.setDisplayName(this.displayName);
        itemMeta.setLore(lore);
        itemMeta.addItemFlags(this.flags.toArray(new ItemFlag[]{}));
        itemMeta.setUnbreakable(this.unbreakable);
        for (Map.Entry<Enchantment, Integer> entry : this.enchantmentIntegerMap.entrySet()) {
            itemMeta.addEnchant(entry.getKey(), entry.getValue(), true);
        }
        stack.setItemMeta(itemMeta);
        return stack;
    }

}
