package ru.ckateptb.tablefeature.config;

import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import ru.ckateptb.tablefeature.utils.ArrayUtils;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.nio.file.Paths;

public abstract class Hocon {
    private HoconConfigurationLoader loader;
    private CommentedConfigurationNode config;

    public Hocon(Plugin plugin, String name) {
        File file = new File(plugin.getDataFolder().getAbsolutePath(), name + ".conf");
        File folder = file.getParentFile();
        if (folder.exists() || folder.mkdirs()) {
            this.loader = createNode(file.getAbsolutePath());
            Bukkit.getScheduler().runTaskLater(plugin, () -> {
                load();
                save();
            }, 1);
        }
    }

    private HoconConfigurationLoader createNode(String path) {
        return HoconConfigurationLoader.builder()
                .setPath(Paths.get(path))
                .setDefaultOptions(ConfigurationOptions.defaults().setShouldCopyDefaults(true))
                .build();
    }

    public void load() {
        try {
            this.config = this.loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        apply((node, field, comment) -> {
            try {
                field.set(this, node.getValue(field.get(this)));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    public void save() {
        apply((node, field, comment) -> {
            try {
                node.setValue(field.get(this));
                if (!comment.isEmpty()) {
                    node.setComment(comment);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        try {
            this.loader.save(this.config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        this.load();
        this.save();
        this.load();
    }

    public void apply(final ConfigListener listener) {
        Class<? extends Hocon> cls = getClass();
        String[] preValue = new String[]{};
        if (cls.isAnnotationPresent(Save.class)) {
            Save annotation = cls.getDeclaredAnnotation(Save.class);
            preValue = annotation.value();
        }
        for (Field field : cls.getDeclaredFields()) {
            field.setAccessible(true);
            String[] value = {};
            String comment = "";
            if (field.isAnnotationPresent(Save.class)) {
                Save annotation = field.getDeclaredAnnotation(Save.class);
                value = annotation.value();
                comment = annotation.comment();
            }
            if (value.length == 0) {
                if (preValue.length == 0) return;
                value = new String[]{field.getName()};
            }
            listener.onInteract(config.getNode(ArrayUtils.concatenate(preValue, value)), field, comment);
        }
    }

    @Target({ElementType.FIELD, ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Save {
        String[] value() default {};

        String comment() default "";
    }

    private interface ConfigListener {
        void onInteract(CommentedConfigurationNode node, Field field, String comment);
    }
}