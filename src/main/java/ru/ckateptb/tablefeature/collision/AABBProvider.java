package ru.ckateptb.tablefeature.collision;

public interface AABBProvider {
    AABB getBounds();

    boolean hasBounds();
}
